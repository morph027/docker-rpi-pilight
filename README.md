# Dockerized Pilight for Raspberry Pi (armhf)

**WORK IN PROGRESS**

Purpose: use with LibreELEC Pi's plus docker addon (as they are already running 24/7).

## Build your own image

```bash
./build.sh
```

## Use prebuilt

If you trust me enough :smiling_imp:, you can just grab my prebuilt images:

```bash
docker pull registry.gitlab.com/morph027/docker-rpi-pilight:8.1.5
```

## Run

* create a valid config file (or copy the default one by running `docker run --rm registry.gitlab.com/morph027/docker-rpi-pilight:8.1.5 cat /etc/pilight/config.json > /storage/config.json`) and mount it inside the container (`-v /storage/config.json:/etc/pilight/config.json`)

```bash
docker run \
-d \
--name pilight \
--init \
--network host \
--restart always \
-v /storage/config.json:/etc/pilight/config.json \
--cap-add SYS_RAWIO \
--device /dev/mem \
registry.gitlab.com/morph027/docker-rpi-pilight:8.1.5
# or use your own image pilight:x.x.x
```
