#!/bin/bash

setup_qemu() {
  sudo apt-get install qemu-user
  which /usr/bin/qemu-arm-static || exit 1
}

which /usr/bin/qemu-arm-static || setup_qemu

TEMP_DIR="$(mktemp -d)"

cp /usr/bin/qemu-arm-static "$TEMP_DIR/"
curl -Lo "$TEMP_DIR/tini" https://github.com/krallin/tini/releases/download/v0.18.0/tini-armhf
chmod +x "$TEMP_DIR/tini"

if docker info | grep -q 'Experimental: true'; then
  cp docker/Dockerfile "$TEMP_DIR/Dockerfile"
else
  sed '/rm -f \/usr\/bin\/qemu-arm-static/d' docker/Dockerfile > "$TEMP_DIR/Dockerfile"
fi

cp docker/entrypoint.sh "$TEMP_DIR/"

docker build -t pilight -f "$TEMP_DIR/Dockerfile" "$TEMP_DIR"
